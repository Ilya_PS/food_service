var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    tray: {type: Object, required: true},
    address: {type: String, required: true},
    name: {type: String, required: true},
    email: {type: String, required: true},
    status: {type: String, required: true, default: 'ожидает подтверждения'}
});

module.exports = mongoose.model('Order', schema);