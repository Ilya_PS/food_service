var express = require('express');
var router = express.Router();
var Tray = require('../models/tray');
var Product = require('../models/product');
var Order = require('../models/order');
const isNumber = require('is-number');

/* GET home page. */
router.get('/', function (req, res, next) {
    var successMsg = req.flash('success')[0];
    Product.find(function (err, docs) {
        var productChunks = [];
        var chunkSize = 3;
        for (var i = 0; i < docs.length; i += chunkSize) {
            productChunks.push(docs.slice(i, i + chunkSize));
        }
        res.render('shop/index', {
            title: 'Столовая Студенческая',
            products: productChunks,
            successMsg: successMsg,
            noMessages: !successMsg
        });
    });
});

router.get('/add-to-tray/:id', function (req, res, next) {
    var productId = req.params.id;
    var tray = new Tray(req.session.tray ? req.session.tray : {});

    Product.findById(productId, function (err, product) {
        if (err) {
            return res.redirect('/');
        }
        tray.add(product, product.id);
        req.session.tray = tray;
        console.log(req.session.tray);
        res.redirect('/');
    });
});

router.get('/addone/:id', function (req, res, next) {
    var productId = req.params.id;
    var tray = new Tray(req.session.tray ? req.session.tray : {});

    tray.addByOne(productId);
    req.session.tray = tray;
    res.redirect('/food-tray');
});

router.get('/reduce/:id', function (req, res, next) {
    var productId = req.params.id;
    var tray = new Tray(req.session.tray ? req.session.tray : {});

    tray.reduceByOne(productId);
    req.session.tray = tray;
    res.redirect('/food-tray');
});

router.get('/remove/:id', function (req, res, next) {
    var productId = req.params.id;
    var tray = new Tray(req.session.tray ? req.session.tray : {});

    tray.removeItem(productId);
    req.session.tray = tray;
    res.redirect('/food-tray');
});

router.get('/food-tray', function (req, res, next) {
    if (!req.session.tray) {
        return res.render('shop/food-tray', {products: null});
    }
    var tray = new Tray(req.session.tray);
    res.render('shop/food-tray', {products: tray.generateArray(), totalPrice: tray.totalPrice});
});

router.get('/menu', isLoggedIn, function (req, res, next) {
    var successMsg = req.flash('success')[0];
    Product.find(function (err, docs) {
        var productChunks = [];
        var chunkSize = 3;
        for (var i = 0; i < docs.length; i += chunkSize) {
            productChunks.push(docs.slice(i, i + chunkSize));
        }
        res.render('shop/menu', {products: productChunks, successMsg: successMsg, noMessages: !successMsg});
    });
});

router.get('/add-to-menu', isLoggedIn, function (req, res, next) {
    var errMsg = req.flash('errMsg')[0];
    res.render('shop/add-to-menu', {errMsg: errMsg, noError: !errMsg});
});

router.post('/add-to-menu', isLoggedIn,
    function (req, res, next) {
        if (!isNumber(req.body.price)) {
            req.flash('errMsg', 'Цена должна быть числом!');

            return res.redirect('/add-to-menu');
        }
        var products = [
            new Product({
                imagePath: req.body.imagePath,
                title: req.body.title,
                description: req.body.description,
                price: parseFloat(req.body.price)
            })
        ];
        products[0].save(function (err, result) {
            req.flash('success', 'Пункт меню добавлен!');
            res.redirect('/menu');
        })
    });

router.get('/delete-from-menu/:id', function (req, res, next) {
    var productId = req.params.id;
    Product.deleteOne({'_id': productId}, function (err, result) {
        req.flash('success', 'Пункт меню удален!');
        res.redirect('/menu');
    });
});

router.get('/status_0/:id', function (req, res, next) {
    var orderId = req.params.id;
    Order.updateOne({_id: orderId}, {
        status: "ожидает подтверждения"
    }, function(err, result) {
        res.redirect('/user/admin-profile');
    })
});
router.get('/status_1/:id', function (req, res, next) {
    var orderId = req.params.id;
    Order.updateOne({_id: orderId}, {
        status: "на кухне"
    }, function(err, result) {
        res.redirect('/user/admin-profile');
    })
});
router.get('/status_2/:id', function (req, res, next) {
    var orderId = req.params.id;
    Order.updateOne({_id: orderId}, {
        status: "доставка"
    }, function(err, result) {
        res.redirect('/user/admin-profile');
    })
});

router.get('/checkout', isLoggedIn, function (req, res, next) {
    if (!req.session.tray) {
        return res.redirect('/food-tray');
    }
    var tray = new Tray(req.session.tray);
    var errMsg = req.flash('error')[0];
    res.render('shop/checkout', {total: tray.totalPrice, errMsg: errMsg, noError: !errMsg});
});

router.post('/checkout', isLoggedIn, function (req, res, next) {
    if (!req.session.tray) {
        return res.redirect('/food-tray');
    }
    var tray = new Tray(req.session.tray);

    var order = new Order({
        user: req.user,
        tray: tray,
        address: req.body.address,
        name: req.body.name,
        email: req.user.email
    });
    order.save(function (err, result) {
        req.flash('success', 'Заказ успешно принят!');
        req.session.tray = null;
        res.redirect('/');
    })
});


module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.url;
    res.redirect('/user/signin');
}
