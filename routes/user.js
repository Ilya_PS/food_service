var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var passport = require('passport');

var Order = require('../models/order');
var Tray = require('../models/tray');
var User = require('../models/user');
var csrfProtection = csrf();


router.use(csrfProtection);

router.get('/admin-profile', isAdminLoggedIn, function (req, res, next) {
    Order.find(function(err, orders) {
        if (err) {
            return res.write('Error!');
        }
        var tray;
        var totalPrice = 0;
        orders.forEach(function(order) {
            tray = new Tray(order.tray);
            order.items = tray.generateArray();
            totalPrice += tray.totalPrice;
        });
        res.render('user/admin-profile', { orders: orders, totalPrice:totalPrice});
    });
});

router.get('/profile', isLoggedIn, function (req, res, next) {
    Order.find({user: req.user}, function(err, orders) {
        if (err) {
            return res.write('Error!');
        }
        var tray;
        orders.forEach(function(order) {
            tray = new Tray(order.tray);
            order.items = tray.generateArray();
        });
        res.render('user/profile', { orders: orders });
    });
});

router.get('/logout', isLoggedIn, function (req, res, next) {
    req.logout();
    req.session.destroy();
    res.redirect('/');
});

router.use('/', notLoggedIn, function (req, res, next) {
    next();
});

router.get('/signup', function (req, res, next) {
    var messages = req.flash('error');
    res.render('user/signup', {csrfToken: req.csrfToken(), messages: messages, hasErrors: messages.length > 0});
});

router.post('/signup', passport.authenticate('local.signup', {
    failureRedirect: '/user/signup',
    failureFlash: true
}), function (req, res, next) {
    if (req.session.oldUrl) {
        var oldUrl = req.session.oldUrl;
        req.session.oldUrl = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/user/profile');
    }
});

router.get('/signin', function (req, res, next) {
    var messages = req.flash('error');
    res.render('user/signin', {csrfToken: req.csrfToken(), messages: messages, hasErrors: messages.length > 0});
});

router.post('/signin', passport.authenticate('local.signin', {
    failureRedirect: '/user/signin',
    failureFlash: true
}), function (req, res, next) {
    if (req.session.oldUrl) {
        var oldUrl = req.session.oldUrl;
        req.session.oldUrl = null;
        res.redirect(oldUrl);
    } else {
        if (req.user.is_admin) {
            res.redirect('/user/admin-profile');
        }else{
            res.redirect('/user/profile');
        }
    }
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function notLoggedIn(req, res, next) {
    if (!req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function isAdminLoggedIn(req, res, next) {
    if (req.isAuthenticated() && req.user.is_admin) {
        return next();
    }
    req.session.oldUrl = req.url;
    res.redirect('/user/admin-profile');
}
