var Product = require('../models/product');

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/foodservice');

var products = [
    new Product({
        imagePath: 'http://budweisbar.dostavkatver.ru/wp-content/uploads/2017/06/DSC03482-2.jpg',
        title: 'Греча',
        description: 'Тает во рту!!!!',
        price: 150
    }),
    new Product({
        imagePath: 'https://www.myasnov.ru/upload/iblock/e51/e510b4d2099d0369eca3cdbd1b0848d2.jpg',
        title: 'Сосиса',
        description: 'Приятная теплость вареной сосисы!!!',
        price: 100
    }),
    new Product({
        imagePath: 'http://f.vkusnota.kz/recipes/018/722/1.jpg',
        title: 'Котлетка',
        description: 'Нежная как утренний бриз!!!',
        price: 50
    }),
    new Product({
        imagePath: 'https://viveris.ru/article_media/112/746319.jpg',
        title: 'Огурчики',
        description: 'Чувствуют себя огурцом !!!',
        price: 20
    }),
    new Product({
        imagePath: 'https://www.gastronom.ru/binfiles/images/20150804/b78c08a9.jpg',
        title: 'Борщ',
        description: 'О да это борщ!!!',
        price: 200
    }),
    new Product({
        imagePath: 'http://www.sergiev-kanon.ru/ai/1000/product.4/sergievskiy-pshenichnyy(pics.5).jpg',
        title: 'Хлеб',
        description: 'Хлеб всему ГОЛОВА!',
        price: 15
    }),
    new Product({
        imagePath: 'https://s2.eda.ru/StaticContent/Photos/120424123536/181127162447/p_O.jpg',
        title: 'Компот',
        description: 'А где компот?',
        price: 50
    }),
    new Product({
        imagePath: 'http://www.kriek.ru/upload/images/pravilno.jpg',
        title: 'Пиво',
        description: 'Это жидкая всему ГОЛОВА!',
        price: 30
    }),
    new Product({
        imagePath: 'http://alcoonline.ru/uploads/images/Stoli/2bottle.jpg',
        title: 'Столичная',
        description: 'Для дома для семьи!',
        price: 150
    })

];

var done = 0;
for (var i = 0; i < products.length; i++) {
    products[i].save(function(err, result) {
        done++;
        if (done === products.length) {
            exit();
        }
    });
}

function exit() {
    mongoose.disconnect();
}