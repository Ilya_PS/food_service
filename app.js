var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressHbs = require('express-handlebars');
var mongoose = require('mongoose');
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var validator = require('express-validator');
var MongoStore = require('connect-mongo')(session);

var routes = require('./routes/index');
var userRoutes = require('./routes/user');
var app = express();

mongoose.connect('mongodb://localhost:27017/foodservice');
require('./config/passport');


// view engine setup
app.engine('.hbs', expressHbs({defaultLayout: 'layout', extname: '.hbs'}));
app.set('view engine', '.hbs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public','favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator());
app.use(cookieParser());
app.use(session({
  // секретный ключ для защиты сессии 
  secret: '8Rlr1HUHRlkCATltPXIVCDBXUfyJtnBG',
  resave: false, 
  saveUninitialized: false,
  // хранит данные пользовательской сессии в развернутой базе mongodb
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
  // объявляет время хранения пользовательских данных в тыс. долях секундах
  // этой инструкцией мы указываем пакету express-session хранить пользовательские данные в течении 3 часов с последствующим удалением
    cookie: { maxAge: 180 * 60 * 1000 }
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
   // определяем есть ли у нас на входе аутентифицированный пользователь
    res.locals.login = req.isAuthenticated();
   // определяем имя(email) если вход в систему совершен 
    res.locals.globalUserName = req.isAuthenticated() ? req.user.email : 'незнакомый комрад';
    // определяем является ли пользователь админом системы  
    res.locals.isAdmin = req.isAuthenticated() ? req.user.is_admin : 0;
    // получаем данные пользовательской сессии(о сохраненных в корзине продуктах)
    res.locals.session = req.session;
    next();
});

app.use('/user', userRoutes);
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
